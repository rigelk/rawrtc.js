## Contributing

The N-API is used through the node-addon-api C++ syntactic sugar over it.

Compilation of the bindings is done via `npm install`.

Compilation of the JavaScript library (written in TypeScript) exporting these bindings in a module is done via `npm run build`. Test the result via `npm test`.
