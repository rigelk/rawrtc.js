const native: any = require("bindings")("rawrtc.js")
const EventEmitter = require('events').EventEmitter

export = () => {
  (<any>Object).assign(new EventEmitter(), {
    // implement the simple-peer api
    close () {},
    RTCPeerConnection () { return native.peerConnection },
    RTCSessionDescription () {},
    RTCIceCandidate () {},
    RTCDataChannel () {}
  })
}