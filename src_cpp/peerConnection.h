#ifndef EXAMPLES_PEER_CONNECTION_H_
#define EXAMPLES_PEER_CONNECTION_H_

#include <node.h>
#include <napi.h>
#include "rawrtc.h"

struct peer_connection_client { // Note: Shadows struct client
    char* name;
    char** ice_candidate_types;
    size_t n_ice_candidate_types;
    bool offering;
    struct rawrtc_peer_connection_configuration* configuration;
    struct rawrtc_peer_connection* connection;
    struct data_channel_helper* data_channel_negotiated;
    struct data_channel_helper* data_channel;
};

class peerConnection : public Napi::ObjectWrap<peerConnection> {
 public:
  static Napi::Object Init(Napi::Env env, Napi::Object exports);
  peerConnection(const Napi::CallbackInfo& info);

 private:
  static Napi::FunctionReference constructor;

  Napi::Value addIceCandidate(const Napi::CallbackInfo& info);
//  Napi::Value addStream(const Napi::CallbackInfo& info);
//  Napi::Value addTrack(const Napi::CallbackInfo& info);
//  Napi::Value close(const Napi::CallbackInfo& info);
//  Napi::Value createAnswer(const Napi::CallbackInfo& info);
//  Napi::Value createDataChannel(const Napi::CallbackInfo& info);
//  Napi::Value createOffer(const Napi::CallbackInfo& info);
//  Napi::Value generateCertificate(const Napi::CallbackInfo& info);
//  Napi::Value getConfiguration(const Napi::CallbackInfo& info);
//  Napi::Value getIdentityAssertion(const Napi::CallbackInfo& info);
//  Napi::Value getLocalStreams(const Napi::CallbackInfo& info);
//  Napi::Value getReceivers(const Napi::CallbackInfo& info);
//  Napi::Value getRemoteStreams(const Napi::CallbackInfo& info);
//  Napi::Value getSenders(const Napi::CallbackInfo& info);
//  Napi::Value getStreamById(const Napi::CallbackInfo& info);
//  Napi::Value removeStream(const Napi::CallbackInfo& info);
//  Napi::Value removeTrack(const Napi::CallbackInfo& info);
//  Napi::Value setConfiguration(const Napi::CallbackInfo& info);
//  Napi::Value setIdentityProvider(const Napi::CallbackInfo& info);
//  Napi::Value setLocalDescription(const Napi::CallbackInfo& info);
//  Napi::Value setRemoteDescription(const Napi::CallbackInfo& info);

//  bool canTrickleIceCandidates;

  char** ice_candidate_types = NULL;
  size_t n_ice_candidate_types = 0;
  enum rawrtc_ice_role role;
  struct rawrtc_peer_connection_configuration* configuration;
  struct peer_connection_client client;

  // inter-thread communication
//  uv_loop_t *loop;
//  uv_async_t async;
};

#endif // EXAMPLES_PEER_CONNECTION_H_
