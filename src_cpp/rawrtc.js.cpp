#include <napi.h>
#include "rawrtc.h"

#include "peerConnection.h"

Napi::Object InitAll(Napi::Env env, Napi::Object exports) {
  return peerConnection::Init(env, exports);
}

NODE_API_MODULE(rawrtcjs, InitAll)
