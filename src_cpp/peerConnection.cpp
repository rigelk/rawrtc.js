#include "helper/common_rawrtc.h"
#include "peerConnection.h"

Napi::FunctionReference peerConnection::constructor;

Napi::Object peerConnection::Init(Napi::Env env, Napi::Object exports) {
  Napi::HandleScope scope(env);

  Napi::Function func = DefineClass(env, "peerConnection", {
    InstanceMethod("addIceCandidate", &peerConnection::addIceCandidate)
  });

  constructor = Napi::Persistent(func);
  constructor.SuppressDestruct();

  exports.Set("peerConnection", func);
  return exports;
}

peerConnection::peerConnection(const Napi::CallbackInfo& info) : Napi::ObjectWrap<peerConnection>(info) {
  Napi::Env env = info.Env();
  Napi::HandleScope scope(env);
}

Napi::Value peerConnection::addIceCandidate(const Napi::CallbackInfo& info) {
  double num = 0;

  return Napi::Number::New(info.Env(), num);
}
