#!/bin/env bash

(
  cd src || exit 1
  if [ ! -d "${PWD}/build/dependencies" ]; then
    ./make-dependencies.sh
  fi
  export PKG_CONFIG_PATH=${PWD}/build/prefix/lib/pkgconfig
  cd build || exist 1
  cmake -DCMAKE_INSTALL_PREFIX="${PWD}/prefix" ..
  make install
)
