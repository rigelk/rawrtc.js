<p align="center">
  <img height="120px" src="https://upload.wikimedia.org/wikipedia/commons/d/d9/Node.js_logo.svg" />&nbsp;&nbsp;&nbsp;&nbsp;
  <img height="120px" src="https://webrtc.org/assets/images/webrtc-logo-vert-retro-dist.svg" />
  <img height="120px" src="https://avatars3.githubusercontent.com/u/24740849?s=400&v=4" />
</p>

# rawrtc.js

Node bindings to the rawrtc WebRTC and ORTC library, using CMake-js and N-API.

## Status

It is a WIP and shouldn't be used in production.

This module intends to compatible with `simple-peer`, and is intended for use with RTCDataChannels as well as the MediaStream API.

## Usage

This library doesn't provide pre-compiled binaries yet, so please check the build dependencies.

`npm install rawrtc`

```typescript
import 'rawrtc'

// uses the same API as the `js-platform/node-webrtc` (also known as `wrtc`) package
var pc = new rawrtc.RTCPeerConnection(config)

// compatible with `feross/simple-peer`
var peer = new SimplePeer({
  initiator: true,
  wrtc: rawrtc
})

// listen for errors
rawrtc.on('error', function (err, source) {
  console.error(err)
})
```

## Build

It uses cmake-js, a nice replacement to the insane gyp, so at least you'll need cmake.
